sap.ui.define(
  ["sap/ui/core/Control", "sap/ui/dom/includeStylesheet", "survey-jquery"],
  function (Control, includeStylesheet, Survey) {
    "use strict";

    return Control.extend("customeronboardingform.control.SurveyContainer", {
      metadata: {
        properties: {
          surveyJson: { type: "object" },
        },
      },

      // constructor: function _constructor(id, settings) {
      //   Control.prototype.constructor.call(this, id, settings);
      // },

      init: function () {
        includeStylesheet(
          sap.ui.require.toUrl("survey-jquery/defaultV2.min.css")
        );
        Survey.StylesManager.applyTheme("defaultV2");
      },

      exit: function () {
        if (this._surveyContainer) {
          this._surveyContainer.destroy();
        }
      },

      onBeforeRendering: function () {
        if (this._surveyContainer) {
          this._surveyContainer.destroy();
        }
      },

      onAfterRendering: function () {
        var oSurveyJson = this.getSurveyJson();
        var oSurvey = new Survey.Model(oSurveyJson);
        this._surveyContainer = oSurvey.render(this.getId());
      },

      renderer: {
        apiVersion: 2,

        render(oRm, oControl) {
          oRm.openStart("div", oControl);
          oRm.openEnd();
          oRm.close("div");
        },
      },
    });
  }
);
