sap.ui.define(
  ["sap/ui/core/mvc/Controller"],
  /**
   * @param {typeof sap.ui.core.mvc.Controller} Controller
   */
  function (Controller) {
    "use strict";

    return Controller.extend("customeronboardingform.controller.View1", {
      onInit: function () {
        this._oComponent = this.getOwnerComponent();
        this._oSurveyJson = this._oComponent.getModel("surveyJson");
        this.getView().setModel(this._oSurveyJson, "surveyJson");

        const surveyJson = {
          elements: [
            {
              name: "FirstName",
              title: "Enter your first name:",
              type: "text",
            },
            {
              name: "LastName",
              title: "Enter your last name:",
              type: "text",
            },
          ],
        };
        this._oSurveyJson.setData(surveyJson);
      },
    });
  }
);
