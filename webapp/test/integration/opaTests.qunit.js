/* global QUnit */

sap.ui.require(["customeronboardingform/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
