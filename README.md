## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Thu Sep 01 2022 09:27:14 GMT+0200 (Central European Summer Time)|
|**App Generator**<br>@sap/generator-fiori-freestyle|
|**App Generator Version**<br>1.7.1|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>simple|
|**Service Type**<br>None|
|**Service URL**<br>N/A
|**Module Name**<br>customeronboardingform|
|**Application Title**<br>Customer Onboarding Form|
|**Namespace**<br>|
|**UI5 Theme**<br>sap_horizon|
|**UI5 Version**<br>1.105.1|
|**Enable Code Assist Libraries**<br>False|
|**Add Eslint configuration**<br>False|

## customeronboardingform

Customer Onboarding Form

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


